from PIL import Image

from ImageFile import ImageFile
import base


def Resize(image):
    currentImage = Image.open(str(image.path) + '/' + str(image.filename))
    newImage = currentImage.resize((int(base.wantedWidth), int(base.wantedHeight)), Image.NEAREST)
    newImage.save(base.targetFolder + '/' + str(base.wantedWidth) + 'x' + str(base.wantedHeight) + image.filename)
    if False:
        pass
    pass
