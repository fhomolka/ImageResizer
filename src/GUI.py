import PySimpleGUI as sg
import os
import base

# def foo():
#    sg.Popup('Hello From PySimpleGUI!', 'This is the shortest GUI program ever!')


def printDone():
    sg.Popup('Task Completed', 'Done!')


class Intro():
    def __init__(self):
        self.layout = [[sg.Text('I have initialized!')],
                       [sg.Button('Um, okay?'), sg.Exit()]]
        window = sg.Window('A window', self.layout)

        while True:
            event, values = window.Read()
            if event is None or event == 'Exit':
                break
            print(event, values)
        pass


class MainWindow():
    def __init__(self):
        self.layout = [[sg.Text('Pick the folder with source images')],
                       [sg.Text('Source Folder', size=(12, 1)), sg.InputText(os.getcwd(), key='srcFolder'), sg.FolderBrowse()],
                       [sg.Text('Target Folder', size=(12, 1)), sg.InputText(key='trgtFolder'), sg.FolderBrowse()],
                       [sg.Text('Width'), sg.InputText('1920', size=(15, 1), key='width'), sg.Text('Height'), sg.InputText('1080', size=(15, 1), key='height')],
                       [sg.Submit(), sg.Cancel()]]
        window = sg.Window("Resize files", self.layout)
        while True:
            event, values = window.Read()
            folder_path = values['srcFolder']
            targetFolder = values['trgtFolder']
            Width = values['width']
            Height = values['height']
            print(folder_path)
            if event is None or event == 'Cancel':
                break
            if event == 'Show':
                # change the "output" element to be the value of "input" element
                window.Element('_OUTPUT_').Update(values['_IN_'])
            if event == 'Submit':
                base.wantedWidth = Width
                base.wantedHeight = Height
                base.sourceFolder = folder_path
                base.targetFolder = targetFolder
                print(folder_path)
                files = os.listdir(folder_path)
                images = []
                for file in files:
                    if '.png' in file or '.jpg' in file or '.jpeg' in file:
                        images.append(file)
                print(files)
                print(images)
                for image in images:
                    base.addImage(image, folder_path)
                break
        pass

