#!/usr/bin/env python

from PIL import Image
import ImageManipulation
import GUI
import base


def main():
    x = GUI.MainWindow()
    base.resizeAllImages()
    GUI.printDone()
    pass


if __name__ == '__main__':
    main()
