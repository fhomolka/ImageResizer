import GUI
import ImageManipulation
import ImageFile

images = []
wantedWidth = 0
wantedHeight = 0
sourceFolder = ''
targetFolder = ''


def addImage(name, path):
    x = ImageFile.ImageFile(name, path)
    images.append(x)
    print('Added {0} to images'.format(x.filename))
    print(x)
    pass


def resizeAllImages():
    for image in images:
        ImageManipulation.Resize(image)
    pass
